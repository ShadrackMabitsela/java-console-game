package za.co.wethinkcode.examples.hangman;

import java.util.Objects;
import java.util.Random;

/**
 * <p><b><i><u>ANSWER CLASS</u></i></b></p>
 * <b>The Answer class has the following responsibilities.</b>
 * <ol>
 *     <li>It knows about a specific value to answer (i.e. a partial or
 *     complete word).</li>
 *     <li>It can be updated with guessed input by the user to create a new
 *     answer.</li>
 *     <li>It can be correct or wrong</li>
 *     <li>It can be the chosen word that must be guessed</li>
 * </ol>
 * <b>The Answer class contains following methods:</b>
 * <dl>
 *   <dt><b>Answer</b>(String value)</dt>
 *   <dd>-> The Answer class's constructor.</dd>
 *   <dt><b>hasLetter</b>(char letter)</dt>
 *   <dd>-> Checks if the word contains the guessed letter.</dd>
 *   <dt><b>generateRandomHint</b>()</dt>
 *   <dd>-> Generate a random hint.</dd>
 *   <dt><b>getHint</b>(Answer guessedAnswer, char guessedLetter)</dt>
 *   <dd>-> Builds up a string to provide a hint of the word.</dd>
 *   <dt><b>isGoodGuess</b>(Answer wordToGuess, char letter)</dt>
 *   <dd>-> Checks if the letter was a good guess.</dd>
 *   <dt><b>toString</b>()</dt>
 *   <dd>-> Overrides the super class's toString() method.</dd>
 *   <dt><b>equals</b>(Object o)</dt>
 *   <dd>-> Overrides the super class's equals(Object o) method.</dd>
 *   <dt><b>hashCode</b>()</dt>
 *   <dd>-> Overrides the super class's hashCode() method.</dd>
 * </dl>
 *
 * @author  Shadrack Mabitsela
 * @version 1.0
 * @since   2021-03-02
 */
public class Answer {
    private final String value;

    /**
     * The Answer class's constructor.
     * @param value Type: String.
     */
    public Answer(String value) {
        this.value = value;
    }

    /**
     * This method checks if the word contains the guessed letter.
     * @param letter Type: char.
     * @return boolean This method returns a boolean value.
     */
    public boolean hasLetter(char letter) {
        return this.value.indexOf(letter) >= 0;
    }

    /**
     * This method gets the initial hint so that the game can start.
     * @return Answer This method returns a value of class type Answer.
     */
    public Answer generateRandomHint() {
        Random random = new Random();
        int randomIndex = random.nextInt(this.value.length() - 1);

        String noLetters = "_".repeat(this.value.length());
        return this.getHint( new Answer(noLetters),
                this.value.charAt(randomIndex));
    }

    /**
     * This method builds up a string to provide a hint of the word.
     * @param guessedAnswer Type: class Answer.
     * @param guessedLetter Type: char.
     * @return Answer This method returns a value of class type Answer.
     */
    public Answer getHint(Answer guessedAnswer, char guessedLetter) {
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < this.value.length(); i++) {
            if (guessedLetter == this.value.charAt(i)) {
                result.append(guessedLetter);
            } else {
                result.append(guessedAnswer.toString().charAt(i));
            }
        }
        return new Answer(result.toString());
    }

    /**
     * This method checks if the letter was a good guess.
     * @param wordToGuess Type: class Answer.
     * @param letter Type: char.
     * @return boolean This method returns a boolean value.
     */
    public boolean isGoodGuess(Answer wordToGuess, char letter) {
        return wordToGuess.hasLetter(letter) && !this.hasLetter(letter);
    }

    /**
     * This method overrides the super class's toString() method.
     * @return String This method returns the String value of the Answer class.
     */
    @Override
    public String toString() {
        return this.value;
    }

    /**
     * This method overrides the super class's equals(Object o) method.
     * @param o Type: class Object.
     * @return boolean This method returns a boolean value for equality.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Answer answer = (Answer) o;
        return value.equals(answer.value);
    }

    /**
     * This method overrides the super class's hashCode() method.
     * @return int This method returns a hashcode integer value.
     */
    @Override
    public int hashCode() { return Objects.hash(value); }
}
