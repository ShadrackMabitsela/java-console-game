package za.co.wethinkcode.examples.hangman;

import java.io.InputStream;
import java.util.Scanner;

/**
 * <p><b><i><u>PLAYER CLASS</u></i></b></p>
 * <b>The Player class has the following responsibilities.</b>
 * <ol>
 *     <li>The Player should start with 5 chances.</li>
 *     <li>The Player should decrease the number of chances each time a wrong
 *     letter is guessed.</li>
 *     <li>The Player should always know the number of chances.</li>
 *     <li>The Player should know when there are no more chances.</li>
 * </ol>
 * <b>The Player class contains following methods:</b>
 * <dl>
 *   <dt><b>Player</b>()</dt>
 *   <dd>-> The Player class's default constructor.</dd>
 *   <dt><b>Player</b>(InputStream in)</dt>
 *   <dd>-> The Player class's overloaded constructor.</dd>
 *   <dt><b>getWordsFile</b>()</dt>
 *   <dd>-> Get the filename input from the terminal/console.</dd>
 *   <dt><b>getChances</b>()</dt>
 *   <dd>-> Gets the number of chances the player has.</dd>
 *   <dt><b>lostChance</b>()</dt>
 *   <dd>-> Decrements the total number of chances by one.</dd>
 *   <dt><b>hasNoChances</b>()</dt>
 *   <dd>-> Checks the number of chances.</dd>
 *   <dt><b>getGuess</b>()</dt>
 *   <dd>-> Gets the player's guess/response and returns a string.</dd>
 *   <dt><b>wantsToQuit</b>()</dt>
 *   <dd>-> Checks if the player wants to quit.</dd>
 * </dl>
 *
 * @author  Shadrack Mabitsela
 * @version 1.0
 * @since   2021-03-02
 */
public class Player {
    private int chances = 5;
    private boolean quit = false;
    private final Scanner scanner;

    /**
     * The Player class's default constructor.
     */
    public Player() {
        this.scanner = new Scanner(System.in);
    }

    /**
     * The Player class's overloaded constructor.
     * @param in Type: InputStream.
     */
    public Player(InputStream in) {
        this.scanner = new Scanner(in);
    }

    /**
     * This method get the filename input from the terminal/console.
     * @return String This method returns the filename string value.
     */
    public String getWordsFile() {
        String filename = scanner.nextLine();
        return filename.isBlank() ? "short_words.txt" : filename;
    }

    /**
     * This method gets the number of chances the player has.
     * @return int Returns the number of chances the player has.
     */
    public int getChances() {
        return this.chances;
    }

    /**
     * This method decrements the total number of chances by one.
     */
    public void lostChance() {
        if (!this.hasNoChances()) {
            this.chances--;
        }
    }

    /**
     * This method checks the number of chances.
     * @return boolean Returns a boolean value based on the number of chances.
     */
    public boolean hasNoChances() {
        return this.getChances() == 0;
    }

    /**
     * This method gets the player's guess/response and returns a string.
     * @return String Returns a string variable 'text'.
     */
    public String getGuess() {
        String text = scanner.nextLine();
        this.quit =
                text.equalsIgnoreCase(
                        "quit") || text.equalsIgnoreCase(
                                "exit");
        return text;
    }

    /**
     * This method checks if the player wants to quit.
     * @return boolean Returns the boolean value of the private field 'quit'.
     */
    public boolean wantsToQuit() {
        return this.quit;
    }
}
