/*-----------------------------------------------------------------------------#
# Program:     Hangman [Java].                                                 #
# Author:      Shadrack Mabitsela [smabitse@student.wethinkcode.co.za].        #
# Created:     28 / 02 / 2021.                                                 #
# Updated:     02 / 03 / 2021  13:00.                                          #
#                                                                              #
# Description: This program simulates the hangman game giving the player       #
#              5 attempts to guess the missing letters in the randomly         #
#              selected word.                                                  #
#              The program contains three classes:                             #
#              Hangman.java, Answer.java and Player.java.                      #
#-----------------------------------------------------------------------------*/
package za.co.wethinkcode.examples.hangman;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Random;

/**
 * <p><b><i><u>HANGMAN CLASS</u></i></b></p>
 * As the Hangman class contains the main() method of the program the Hangman
 * class is the entry point of the program.<br/>
 * <b>The Hangman class contains the following methods:</b>
 * <dl>
 *   <dt><b>main</b>(String[] args)</dt>
 *   <dd>-> The entry point and calls all the relevant methods/classes.</dd>
 *   <dt><b>start</b>(Answer wordToGuess)</dt>
 *   <dd>-> Gets the initial hint so that the game can start.</dd>
 *   <dt><b>run</b>(Player player,Answer wordToGuess,Answer currentAnswer)</dt>
 *   <dd>-> Runs the game loop.</dd>
 *   <dt><b>getWords</b>(Player player)</dt>
 *   <dd>-> Gets a number of words from a file containing random words.</dd>
 * </dl>
 *
 * @author  Shadrack Mabitsela
 * @version 1.0
 * @since   2021-03-02
 */
public class Hangman {
    /**
     * The programs entry point and it calls all the relevant methods/classes.
     * @param args Type: String array.
     * @exception IOException On input error.
     * @see IOException
     */
    public static void main(String[] args) throws IOException {
        var player = new Player();
        var wordToGuess = getWords(player);
        var currentAnswer = start(wordToGuess);
        String message = run(player, wordToGuess, currentAnswer);
        System.out.println(message);
    }

    /**
     * This method gets the initial hint so that the game can start.
     * @param wordToGuess Type: class Answer.
     * @return Answer This method returns a value of class type Answer.
     */
    private static Answer start(Answer wordToGuess) {
        var currentAnswer = wordToGuess.generateRandomHint();
        System.out.println("Guess the word: " + currentAnswer);
        return currentAnswer;
    }

    /**
     * This method runs the hangman game loop.
     * @param player Type: class Player.
     * @param wordToGuess Type: class Answer.
     * @param currentAnswer Type: class Answer.
     * @return String This method returns a string message to the main method.
     */
    private static String run(Player player, Answer wordToGuess,
                            Answer currentAnswer) {
        while (!currentAnswer.equals(wordToGuess)) {
            String guess = player.getGuess();
            if (player.wantsToQuit()) { return "Bye!";}

            char guessedLetter = guess.charAt(0);
            if (currentAnswer.isGoodGuess(wordToGuess, guessedLetter)) {
                currentAnswer = wordToGuess.getHint(currentAnswer,
                        guessedLetter);
                System.out.println(currentAnswer);
            } else {
                player.lostChance();
                System.out.println(
                        "Wrong! Number of guesses left: "
                                + player.getChances());
                if (player.hasNoChances()) {
                    return "Sorry, you are out of guesses. The word was: "
                            + wordToGuess;
                }
            }
        }
        return "That is correct! You escaped the noose .. this time.";
    }

    /**
     * This method gets a number of words from a file containing random words.
     * @param player Type: class Player.
     * @return Answer This method returns a value of class type Answer.
     * @exception IOException On input error.
     * @see IOException
     */
    private static Answer getWords(Player player) throws IOException {
        Random random = new Random();

        System.out.println("Words file? [leave empty to use short_words.txt]");
        String fileName = player.getWordsFile();

        List<String> words = Files.readAllLines(Path.of(fileName));

        int randomIndex = random.nextInt(words.size());
        String randomWord = words.get(randomIndex).trim();
        return new Answer(randomWord);
    }
}
